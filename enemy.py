import pygame
from consts import *


class Enemy(pygame.Rect):
    def __init__(self, x, y, type):
        self.type = str(type)
        self.x = x
        self.y = y
        if type == "1":
            self.w = 98
            self.h = 50
        else:
            self.w = 91
            self.h = 91
            
        self.direction = "left"
        
    def move(self):
        if self.direction == "left":
            min_x = BORDER
            self.x -= ENEMY_SPEED
            if self.x <= min_x:
                self.direction = "right"
                self.y += self.h / 2
                self.x = min_x
        else:
            max_x = DRAW_SCREEN_SIZE[0] - BORDER - self.w
            self.x += ENEMY_SPEED
            if self.x >= max_x:
                self.direction = "left"
                self.y += self.h / 2
                self.x = DRAW_SCREEN_SIZE[0] - BORDER - self.w

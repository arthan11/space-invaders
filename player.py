import pygame
from consts import *


class Player(pygame.Rect):
    def __init__(self):
        self.w = 99
        self.h = 75
        self.x = int(round((DRAW_SCREEN_SIZE[0]-self.w)/2))
        self.y = DRAW_SCREEN_SIZE[1] - self.h - BORDER
        self.hp = 3

import os
import sys
from random import randint
import pygame
from consts import *
from player import Player
from enemy import Enemy
from projectile import Projectile

class Game():
    def __init__(self):
        pygame.init()
    
        self.load_textures()
        self.load_sounds()
    
        self.screen = pygame.display.set_mode(SCREEN_SIZE)
        self.draw_screen = pygame.Surface(DRAW_SCREEN_SIZE)
        self.clock = pygame.time.Clock()
        self.dt = 1

        self.font = pygame.font.Font("Mitochondria.ttf", 80)
        
        self.game()
    
    def load_textures(self):
        self.textures = {}
        for img in os.listdir("imgs"):
            texture = pygame.image.load("imgs/" + img)
            self.textures[img.replace(".png", "")] = texture
    
    def load_sounds(self):
        self.sounds = {}
        for sound in os.listdir("sounds"):
            file = pygame.mixer.Sound("sounds/" + sound)
            file.set_volume(0.2)
            self.sounds[sound.replace(".wav", "")] = file
    
    def check_events(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                self.close()
            elif event.type == self.ENEMYMOVE:
                for enemy in self.enemies:
                    enemy.move()
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                self.sounds["shoot"].play()
                projectile = Projectile(self.player.centerx, self.player.centery, "1")
                self.projectiles.append(projectile)
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_LEFT:
                self.player_texture_name = "playerLeft"
            elif event.type == pygame.KEYDOWN and event.key == pygame.K_RIGHT:
                self.player_texture_name = "playerRight"
            elif event.type == pygame.KEYUP and event.key in [pygame.K_RIGHT, pygame.K_LEFT]:
                self.player_texture_name = "player"

    def check_keys(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_RIGHT]:
            max_x = DRAW_SCREEN_SIZE[0] - self.player.w - BORDER
            if self.player.x < max_x:
                self.player.x += int(round(PLAYER_SPEED * self.dt))
            else:
                self.player.x = max_x
        if keys[pygame.K_LEFT]:
            min_x = BORDER
            if self.player.x > min_x:
                self.player.x -= int(round(PLAYER_SPEED * self.dt))
            else:
                self.player.x = min_x
            

    def close(self):
        pygame.quit()
        sys.exit()    
    
    def game(self):
        self.ENEMYMOVE = pygame.USEREVENT
        pygame.time.set_timer(self.ENEMYMOVE, MOVE_RATIO)
    
        self.enemies = []
        ship_space_x = 100
        ship_space_y = 0
        types = [1, 1, 1]
        for y in range(3):
            for x in range(int((DRAW_SCREEN_SIZE[0] - (BORDER * 2)) / ship_space_x)):
                enemy = Enemy(x * ship_space_x + BORDER, y * ship_space_y, types[y])
                enemy.x += enemy.w * x 
                enemy.y += enemy.h * y
                self.enemies.append(enemy)
    
        self.player = Player()
        self.player_texture_name = "player"
        
        self.projectiles = []
    
        while True:
            self.check_keys()
            self.check_events()
            
            for projectile in self.projectiles:
                projectile.move()
                if projectile.y < 0 or projectile.y > DRAW_SCREEN_SIZE[1]:
                    self.projectiles.remove(projectile)
                if projectile.type == "2" and projectile.colliderect(self.player):
                    self.sounds["explosion"].play()
                    self.projectiles.remove(projectile)
                    self.player.hp -= 1
                elif projectile.type == "1": 
                    for enemy in self.enemies:
                        if projectile.colliderect(enemy):
                            self.sounds["invaderkilled"].play()
                            self.projectiles.remove(projectile)
                            self.enemies.remove(enemy)
                            break
               
            for enemy in self.enemies:
                if enemy.y >= DRAW_SCREEN_SIZE[1] - self.player.h - enemy.h:
                    self.end("GAME OVER")
               
                if randint(1, ENEMY_SHOT_RATIO) == 1:
                    self.sounds["shoot"].play()
                    projectile = Projectile(enemy.centerx, enemy.centery, "2")
                    self.projectiles.append(projectile)
               
            if self.player.hp <= 0:
                self.end("GAME OVER")

            if len(self.enemies) == 0:
                self.end("PLAYER WINS!")
               
            self.draw()
            self.refresh_screen()
    
    def end(self, text):
        if text == "GAME OVER":
            self.player_texture_name = "playerDamaged"
        self.draw()
        
        surface = self.font.render(text, False, (255, 255, 255))
        rect = surface.get_rect(center=(DRAW_SCREEN_SIZE[0]/2, DRAW_SCREEN_SIZE[1]/2))
        
        self.draw_screen.blit(surface, rect)
        self.refresh_screen()
        timer = END_TIME
        while timer > 0:
            timer -= self.dt
            self.refresh_screen()
        self.close()
    
    def draw_background(self):
        for y in range(0, DRAW_SCREEN_SIZE[1], 256):
            for x in range(0, DRAW_SCREEN_SIZE[0], 254):
                self.draw_screen.blit(self.textures["background"], (x, y))
    
    def draw(self):
        self.draw_background()
        self.draw_screen.blit(self.textures[self.player_texture_name], self.player)
        for enemy in self.enemies:
            self.draw_screen.blit(self.textures["enemy" + enemy.type], enemy)
        for projectile in self.projectiles:
            self.draw_screen.blit(self.textures["projectile" + projectile.type], projectile)
    
        for life in  range(self.player.hp):
            self.draw_screen.blit(self.textures["life"], (BORDER + 45 * life, BORDER))
    
    def refresh_screen(self):
        scaled = pygame.transform.scale(self.draw_screen, SCREEN_SIZE)
        self.screen.blit(scaled, (0, 0))
        pygame.display.update()
        self.dt = self.clock.tick(FRAMERATE) * FRAMERATE / 1000
    
if __name__ == '__main__':
    Game()